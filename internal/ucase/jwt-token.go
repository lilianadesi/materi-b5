package ucase

import (
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/middleware"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/ucase/contract"
)

type jwtToken struct{}

func NewJwt() contract.UseCase {
	return &jwtToken{}
}

func (jt *jwtToken) Serve(data *appctx.Data) appctx.Response {
	postmanSignature := data.Request.Header.Get("Signature")
	postmanApiKeyId := data.Request.Header.Get("X-Api-Key-ID")
	postmanTimestamp := data.Request.Header.Get("Timestamp")

	fmt.Println("postmanSignature: ", postmanSignature)
	fmt.Println("postmanApiKeyId: ", postmanApiKeyId)
	fmt.Println("postmanTimestamp: ", postmanTimestamp)

	if postmanSignature == "" {
		return *appctx.NewResponse().
			WithStatus("failed").
			WithEntity("generateToken").
			WithState("generateTokenFailed").
			WithCode(http.StatusUnauthorized).
			WithMessage("Signature Not Found")
	}

	if postmanApiKeyId == "" {
		return *appctx.NewResponse().
			WithStatus("failed").
			WithEntity("generateToken").
			WithState("generateTokenFailed").
			WithCode(http.StatusUnauthorized).
			WithMessage("X-Api-Key-ID Not Found")
	}

	if postmanTimestamp == "" {
		return *appctx.NewResponse().
			WithStatus("failed").
			WithEntity("generateToken").
			WithState("generateTokenFailed").
			WithCode(http.StatusBadRequest).
			WithMessage("Timestamp Not Found")
	}

	/* handling hmac */
	serverSignature := middleware.GenerateServerSignature(postmanApiKeyId, postmanTimestamp, data.Request, data.Config)
	isValid := middleware.ValidateHmac(postmanSignature, serverSignature)

	fmt.Println("=============Test HMAC==============")
	fmt.Println("signature server :", serverSignature)
	fmt.Println("========================================")
	fmt.Println("signature client :", postmanSignature)
	fmt.Println("========================================")
	fmt.Println("is HMAC Equal : ", isValid)
	fmt.Println("========================================")

	if !isValid {
		return *appctx.NewResponse().
			WithStatus("failed").
			WithEntity("generateToken").
			WithState("generateTokenFailed").
			WithCode(http.StatusUnauthorized).
			WithMessage("Signature Invalid")
	}

	/* handling jwt */
	token, err := middleware.GenerateJWT()
	if err != nil {
		return *appctx.NewResponse().
			WithStatus("error generating token").
			WithCode(http.StatusInternalServerError).
			WithError(err)
	}

	meta := map[string]interface{}{
		"transaction_id": uuid.New(),
	}

	dataResponse := map[string]interface{}{
		"type":       "Bearer",
		"token":      token,
		"expired_at": time.Now().Add(time.Hour * 2),
	}

	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithStatus("SUCCESS").
		WithEntity("generateToken").
		WithState("generateTokenSuccess").
		WithMessage("Generate Token Success").
		WithMeta(meta).
		WithData(dataResponse)
}

